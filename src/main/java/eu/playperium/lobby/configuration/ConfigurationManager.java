package eu.playperium.lobby.configuration;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;

import java.io.File;
import java.io.IOException;

public class ConfigurationManager {

    private File configurationFile;
    private ConfigurationLoader<CommentedConfigurationNode> configurationLoader;

    private CommentedConfigurationNode configurationNode = null;

    public ConfigurationManager(File configurationFile, ConfigurationLoader<CommentedConfigurationNode> configurationLoader) {
        this.configurationFile = configurationFile;
        this.configurationLoader = configurationLoader;
    }

    public void initialize() {
        try {
            if (!configurationFile.exists()) {
                configurationFile.createNewFile();

                configurationNode = configurationLoader.load();

                configurationNode.getNode("lobby").setValue(null);

                configurationNode.getNode("lobby", "spawn", "x").setValue(null);
                configurationNode.getNode("lobby", "spawn", "y").setValue(null);
                configurationNode.getNode("lobby", "spawn", "z").setValue(null);

                configurationNode.getNode("lobby").setComment("PlayPerium Lobby Default Configuration");

                configurationLoader.save(configurationNode);
            }
            configurationNode = configurationLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CommentedConfigurationNode getConfigurationNode() {
        return this.configurationNode;
    }

    public void saveConfiguration() {
        try {
            this.configurationLoader.save(this.configurationNode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
