package eu.playperium.lobby.inventory;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.Hotbar;
import org.spongepowered.api.item.inventory.property.SlotIndex;

import java.util.ArrayList;

public class HotbarManager {

    private ArrayList<ItemStack> items = new ArrayList<>();

    public void addItem(ItemStack itemStack) {
        items.add(itemStack);
    }

    public void setPlayerHotbar(Player player) {
        // ToDo: "query" is Deprecated!
        Hotbar hotbar = player.getInventory().query(Hotbar.class);

        for (int i = 0; i < items.size(); i++) {
            hotbar.set(new SlotIndex(i), items.get(i));
        }
    }
}
