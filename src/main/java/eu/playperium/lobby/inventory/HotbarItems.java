package eu.playperium.lobby.inventory;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public final class HotbarItems {

    public static final ItemStack mainMenu() {
        ItemStack mainMenu = ItemStack.builder().itemType(ItemTypes.COMPASS).build();
        mainMenu.offer(Keys.DISPLAY_NAME, Text.of(TextColors.GREEN, "Hauptmenü"));

        return mainMenu;
    }
}
