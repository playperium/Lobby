package eu.playperium.lobby.inventory;

import com.google.common.collect.Lists;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.List;

public final class InventoryItems {

    public static final ItemStack teleportSpawn() {
        ItemStack mainMenu = ItemStack.builder().itemType(ItemTypes.BOOKSHELF).build();
        mainMenu.offer(Keys.DISPLAY_NAME, Text.of(TextColors.GREEN, "Spawn"));

        List<Text> lore = Lists.newArrayList(
                Text.of(" "),
                Text.of(TextColors.GRAY, "Teleportiert dich zum Spawn der Lobby."),
                Text.of(" "));

        mainMenu.offer(Keys.ITEM_LORE, lore);

        return mainMenu;
    }

    public static final ItemStack serverFTBContinuum() {
        ItemStack mainMenu = ItemStack.builder().itemType(ItemTypes.FURNACE).build();
        mainMenu.offer(Keys.DISPLAY_NAME, Text.of(TextColors.GREEN, "FTB Continuum"));

        List<Text> lore = Lists.newArrayList(
                Text.of(TextColors.DARK_GRAY, "Survival"),
                Text.of(" "),
                Text.of(TextColors.GRAY, "Stelle deine Fähigkeiten als Ingenieur auf die Probe"),
                Text.of(TextColors.GRAY, "und forsche an den Grenzen der Wissenschaft!"),
                Text.of(" "),
                Text.of(TextColors.AQUA, "Modpack Version: 1.5.2"),
                Text.of(" "),
                Text.of(TextColors.GREEN, "➜ Klicke zum verbinden"),
                Text.of(" "));

        mainMenu.offer(Keys.ITEM_LORE, lore);

        return mainMenu;
    }

    public static final ItemStack serverFTBUltimate() {
        ItemStack mainMenu = ItemStack.builder().itemType(ItemTypes.ENDER_CHEST).build();
        mainMenu.offer(Keys.DISPLAY_NAME, Text.of(TextColors.GREEN, "FTB Ultimate"));

        List<Text> lore = Lists.newArrayList(
                Text.of(TextColors.DARK_GRAY, "Survival"),
                Text.of(" "),
                Text.of(TextColors.GRAY, "Forsche als Techniker oder Magier über die Grenzen"),
                Text.of(TextColors.GRAY, "hinaus und eröffne dir damit neue Wege!"),
                Text.of(" "),
                Text.of(TextColors.AQUA, "Modpack Version: 1.0.1"),
                Text.of(" "),
                Text.of(TextColors.GREEN, "➜ Klicke zum verbinden"),
                Text.of(" "));

        mainMenu.offer(Keys.ITEM_LORE, lore);

        return mainMenu;
    }
}
