package eu.playperium.lobby.commands;

import eu.playperium.lobby.permissions.PluginPermissions;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.chat.ChatTypes;

public class CommandSetSpawn implements CommandExecutor {

    private eu.playperium.lobby.Lobby plugin;

    public CommandSetSpawn (eu.playperium.lobby.Lobby lobby) {
        this.plugin = lobby;
    }

    public CommandSpec commandSpec = CommandSpec.builder().permission(PluginPermissions.COMMAND_SET_SPAWN).executor(this).build();

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;

            String worldname = Sponge.getServer().getDefaultWorldName();

            double x = player.getLocation().getX();
            double y = player.getLocation().getY();
            double z = player.getLocation().getZ();

            double rotX = player.getRotation().getX();
            double rotY = player.getRotation().getY();
            double rotZ = player.getRotation().getZ();

            plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "world").setValue(worldname);

            plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "x").setValue(x);
            plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "y").setValue(y);
            plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "z").setValue(z);

            plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "rotation", "x").setValue(rotX);
            plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "rotation", "y").setValue(rotY);
            plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "rotation", "z").setValue(rotZ);

            plugin.configurationManager.saveConfiguration();

            player.sendMessage(ChatTypes.CHAT, Text.builder("Spawn location set!").build());
        }

        return CommandResult.success();
    }
}
