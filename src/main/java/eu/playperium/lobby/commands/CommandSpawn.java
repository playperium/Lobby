package eu.playperium.lobby.commands;

import com.flowpowered.math.vector.Vector3d;
import eu.playperium.lobby.permissions.PluginPermissions;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class CommandSpawn implements CommandExecutor {

    private eu.playperium.lobby.Lobby plugin;

    public CommandSpawn (eu.playperium.lobby.Lobby lobby) {
        this.plugin = lobby;
    }

    public CommandSpec commandSpec = CommandSpec.builder().permission(PluginPermissions.COMMAND_SPAWN).executor(this).build();

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;

            String worldname = plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "world").getString();

            double x = plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "x").getDouble();
            double y = plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "y").getDouble();
            double z = plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "z").getDouble();

            double rotX = plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "rotation", "x").getDouble();
            double rotY = plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "rotation", "y").getDouble();
            double rotZ = plugin.configurationManager.getConfigurationNode().getNode("lobby", "spawn", "rotation", "z").getDouble();

            World world = Sponge.getServer().getWorld(worldname).get();

            Location<World> location = new Location(world, x, y, z);
            Vector3d rotation = new Vector3d(rotX, rotY, rotZ);

            player.setLocation(location);
            player.setRotation(rotation);
        }

        return CommandResult.success();
    }
}
