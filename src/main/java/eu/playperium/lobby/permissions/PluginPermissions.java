package eu.playperium.lobby.permissions;

public final class PluginPermissions {

    public static final String LOBBY_BREAK_BLOCKS = "pp.lobby.breakblocks";

    // Command Permissions
    public static final String COMMAND_SET_SPAWN = "pp.lobby.command.setspawn";
    public static final String COMMAND_SPAWN = "pp.lobby.command.spawn";
}
