package eu.playperium.lobby;

import com.google.inject.Inject;
import eu.playperium.lobby.commands.CommandSetSpawn;
import eu.playperium.lobby.commands.CommandSpawn;
import eu.playperium.lobby.configuration.ConfigurationManager;
import eu.playperium.lobby.listeners.BlockChangeListener;
import eu.playperium.lobby.listeners.ConnectionListener;
import eu.playperium.lobby.listeners.EntityListener;
import eu.playperium.lobby.listeners.ItemInteractListener;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.network.ChannelBinding;
import org.spongepowered.api.plugin.Plugin;

import java.io.File;

@Plugin(
        id = "lobby",
        name = "Lobby"
)
public class Lobby {

    public ChannelBinding.RawDataChannel bungeeCordChannel;

    @Inject
    private Logger logger;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private File configurationFile = null;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configurationLoader = null;

    public ConfigurationManager configurationManager;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        configurationManager = new ConfigurationManager(configurationFile, configurationLoader);

        configurationManager.initialize();

        registerListeners();
        registerCommands();

        bungeeCordChannel = Sponge.getChannelRegistrar().getOrCreateRaw(this, "BungeeCord");
    }

    private void registerListeners() {
        Sponge.getEventManager().registerListeners(this, new BlockChangeListener());
        Sponge.getEventManager().registerListeners(this, new ConnectionListener());
        Sponge.getEventManager().registerListeners(this, new EntityListener());
        Sponge.getEventManager().registerListeners(this, new ItemInteractListener(this));
    }

    private void registerCommands() {
        Sponge.getCommandManager().register(this, new CommandSetSpawn(this).commandSpec, "setspawn");
        Sponge.getCommandManager().register(this, new CommandSpawn(this).commandSpec, "spawn");
    }
}
