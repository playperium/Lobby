package eu.playperium.lobby.listeners;

import eu.playperium.lobby.permissions.PluginPermissions;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;

import java.lang.annotation.Annotation;

public class BlockChangeListener implements Listener {

    @Override
    public Order order() {
        return null;
    }

    @Override
    public boolean beforeModifications() {
        return false;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break e) {
        if (e.getCause().first(Player.class).isPresent()) {
            Player player = e.getCause().first(Player.class).get();

            BlockPlayerBlockInteraction(player, e);
        }
    }

    @Listener
    public void onBlockPlace(ChangeBlockEvent.Place e) {
        if (e.getCause().first(Player.class).isPresent()) {
            Player player = e.getCause().first(Player.class).get();

            BlockPlayerBlockInteraction(player, e);
        }
    }

    @Listener
    public void onBlockModify(ChangeBlockEvent.Modify e) {
        if (e.getCause().first(Player.class).isPresent()) {
            Player player = e.getCause().first(Player.class).get();

            BlockPlayerBlockInteraction(player, e);
        }
    }

    private void BlockPlayerBlockInteraction(Player player, ChangeBlockEvent e) {
        if (!player.hasPermission(PluginPermissions.LOBBY_BREAK_BLOCKS)) {
            e.setCancelled(true);
        } else {
            if (player.getGameModeData().get(Keys.GAME_MODE).get() != GameModes.CREATIVE) {
                e.setCancelled(true);
            }
        }
    }
}
