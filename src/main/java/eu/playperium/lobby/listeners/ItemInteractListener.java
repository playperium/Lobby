package eu.playperium.lobby.listeners;

import eu.playperium.lobby.inventory.HotbarItems;
import eu.playperium.lobby.inventory.InventoryItems;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.item.inventory.*;
import org.spongepowered.api.item.inventory.property.InventoryTitle;
import org.spongepowered.api.item.inventory.property.SlotPos;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.chat.ChatTypes;
import org.spongepowered.api.text.format.TextColors;

import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

public class ItemInteractListener implements Listener {

    private eu.playperium.lobby.Lobby plugin;

    public ItemInteractListener (eu.playperium.lobby.Lobby lobby) {
        this.plugin = lobby;
    }

    @Override
    public Order order() {
        return null;
    }

    @Override
    public boolean beforeModifications() {
        return false;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }

    @Listener
    public void onItemInteract(InteractItemEvent e) {
        Player player = e.getCause().first(Player.class).get();

        if (!e.getItemStack().getType().getName().equals("minecraft:air")) {
            if (e.getItemStack().get(Keys.DISPLAY_NAME).equals(HotbarItems.mainMenu().get(Keys.DISPLAY_NAME))) {
                e.setCancelled(true);

                Inventory mainMenuInventory = Inventory.builder()
                        .of(InventoryArchetypes.DOUBLE_CHEST)
                        .property(InventoryTitle.of(Text.of(TextColors.DARK_GRAY, "Hauptmenü"))).listener(ClickInventoryEvent.class, this::onItemClick)
                        .build(plugin);

                mainMenuInventory.query(SlotPos.of(0,1)).set(InventoryItems.teleportSpawn());
                mainMenuInventory.query(SlotPos.of(2,1)).set(InventoryItems.serverFTBContinuum());
                mainMenuInventory.query(SlotPos.of(3,1)).set(InventoryItems.serverFTBUltimate());

                player.openInventory(mainMenuInventory);
            }
        }
    }

    @Listener
    public void onItemDrop(DropItemEvent e) {
        e.setCancelled(true);
    }

    private void onItemClick(ClickInventoryEvent e) {
        Player player = e.getCause().first(Player.class).get();
        Transaction<ItemStackSnapshot> transaction = e.getTransactions().get(0);

        e.setCancelled(true);

        if (transaction.getOriginal().get(Keys.DISPLAY_NAME).equals(InventoryItems.teleportSpawn().get(Keys.DISPLAY_NAME))) {
            player.closeInventory();
            Sponge.getCommandManager().process(player, "spawn");
        }

        if (transaction.getOriginal().get(Keys.DISPLAY_NAME).equals(InventoryItems.serverFTBContinuum().get(Keys.DISPLAY_NAME))) {
           connectToServer(player, true, "ftb");
        }
    }

    private void connectToServer(Player player, boolean modded, String server) {
        if (modded) {

            // Prevent Client to crash because of opened Inventory on Connect to other Server.
            // ToDo: Investigate Later!
            Task.Builder taskBuilder = Task.builder();

            taskBuilder.execute(
                    task -> {
                        player.closeInventory();
                        // player.getInventory().clear();

                        // Text itemRemovedMessage = Text.builder("Deine Items in der Lobby wurden entfern aufgrund eines Bugs den wir nicht selber beheben können! Sobald du dich erneut mit der Lobby verbindest, bekommst du die Items wieder!").color(TextColors.RED).build();

                        // player.sendMessage(ChatTypes.CHAT, itemRemovedMessage);

                        task.cancel();
                    }
            ).delay(1, TimeUnit.SECONDS).submit(plugin);

            taskBuilder.execute(
                    task -> {
                        plugin.bungeeCordChannel.sendTo(player, buffer -> buffer.writeUTF("Connect").writeUTF(server));

                        task.cancel();
                    }
            ).delay(2, TimeUnit.SECONDS).submit(plugin);
        } else {
            plugin.bungeeCordChannel.sendTo(player, buffer -> buffer.writeUTF("Connect").writeUTF(server));
        }
    }
}
