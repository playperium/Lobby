package eu.playperium.lobby.listeners;

import eu.playperium.lobby.inventory.HotbarItems;
import eu.playperium.lobby.inventory.HotbarManager;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.inventory.Inventory;

import java.lang.annotation.Annotation;

public class ConnectionListener implements Listener {

    @Override
    public Order order() {
        return null;
    }

    @Override
    public boolean beforeModifications() {
        return false;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }

    @Listener
    public void onClientJoin(ClientConnectionEvent.Join e) {
        e.setMessageCancelled(true);

        if (e.getCause().first(Player.class).isPresent()) {
            Player player = e.getCause().first(Player.class).get();

            if (!player.hasPlayedBefore()) {
                Sponge.getCommandManager().process(player, "spawn");
            }

            setPlayerHotbar(player);
            player.offer(Keys.GAME_MODE, GameModes.ADVENTURE);
        }
    }

    @Listener
    public void onClientQuit(ClientConnectionEvent.Disconnect e) {
        e.setMessageCancelled(true);
    }

    private void setPlayerHotbar(Player player) {
        Inventory inventory = player.getInventory();
        inventory.clear();

        HotbarManager hotbarManager = new HotbarManager();
        hotbarManager.addItem(HotbarItems.mainMenu());

        hotbarManager.setPlayerHotbar(player);
    }
}
