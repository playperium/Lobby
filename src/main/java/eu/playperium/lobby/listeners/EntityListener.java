package eu.playperium.lobby.listeners;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.entity.DamageEntityEvent;

import java.lang.annotation.Annotation;

public class EntityListener implements Listener {

    @Override
    public Order order() {
        return null;
    }

    @Override
    public boolean beforeModifications() {
        return false;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }

    @Listener
    public void onEntityDamage(DamageEntityEvent e) {
        if(e.getTargetEntity() instanceof Player) {
            e.setCancelled(true);
        }
    }
}
